import datetime

from google.appengine.ext import db

class Post(db.Model):
	title = db.StringProperty(required= True)
	text = db.TextProperty(required= True)
	created = db.DateTimeProperty(auto_now_add = True)
	price = db.FloatProperty()
	owner_name = db.StringProperty(required= True)
	owner_id = db.IntegerProperty(required= True)
	vouchers = db.StringListProperty()
	address = db.StringProperty()
	geo_lat = db.StringProperty()
	geo_long = db.StringProperty()
	selected = db.StringProperty()
	status = db.StringProperty(required=True, \
								choices=set(['Active', 'In progress', 'Done']))

	@property
	def id(self):
		return self.key().id()

	@classmethod
	def create(cls, title, text, price, owner, address, lat, lon):
		p = Post(title = title, text = text, price = float(price), \
				geo_lat = lat, geo_long = lon,\
				owner_id = owner.id, owner_name = owner.name,\
				address = address, vouch_count = 0, vouchers = [], 
				status = 'Active', selected = '')
		return p

	def inf(self):
		return "%s, %s" % (str(self.id), self.title)

	def vouchers_names(self):
		return [d.split(", ")[1] for d in self.vouchers]

	def vouchers_ids(self):
		return [int(d.split(", ")[0]) for d in self.vouchers]

	def vouchers_prep(self):
		return zip(self.vouchers_ids(), self.vouchers_names())

	def timestamp(self):
		epoch = datetime.datetime.utcfromtimestamp(0)
		delta = self.created - epoch
		return delta.total_seconds()

	def as_dict(self):
		post_dict = {"id": self.id,
					 "title": self.title,
					 "text": self.text,
					 "created": self.timestamp(),
					 "owner_name": self.owner_name,
					 "owner_id": self.owner_id,
					 "vouchers": self.vouchers,
					 "address": self.address,
					 "lat": self.geo_lat,
					 "lon": self.geo_long,
					 "selected": self.selected,
					 "status": self.status}
		return post_dict

	@property
	def created_datetime(self):
	    return self.created.strftime(format = "%a %d %b %Y on %H:%M")
	    # return self.created.strftime(format = "%H:%M")

