$("document").ready(function(){

	var filter = function(query) {
		$(".filter").show();

		if (query != "") {
			$(".filter:not(."+query+")").hide();
		}
	}

	// filter(window.location.hash);
	
	/* Get user coords */
	var getLocation = function() {
		var lat, lng;

		if ( navigator.geolocation ) {
			navigator.geolocation.getCurrentPosition(function(position) {
				
				/* Clear input fields */
				$("input[name='address']").val("");
				$("input[name='latitude']").val("");
				$("input[name='longitude']").val("");

				lat = position.coords.latitude;
				lng = position.coords.longitude;

				var address;
				var geocoder = new google.maps.Geocoder();
				var latlng = new google.maps.LatLng(lat, lng);
				geocoder.geocode({"latLng": latlng}, function(results, status){
					if (status == google.maps.GeocoderStatus.OK) {
						if (results[0]) {
							//alert(results[0].formatted_address);
							address = results[0].address_components[1].short_name + " " + results[0].address_components[0].short_name;

							$("input[name='address']").val(address);
							$("input[name='latitude']").val(lat);
							$("input[name='longitude']").val(lng);
							$(".post-submit").prop("disabled", false).removeClass("btn-grey").addClass("btn-primary");
							$("form").find(".spinner").removeClass("spinner");
						
						} else {
							alert("No results found");
						}

					} else {
						alert("Geocoder failed due to: " + status);
					}
				});

			}, function(error) {
				alert(error.message);

				$(".post-submit").prop("disabled", false).removeClass("btn-grey").addClass("btn-primary");
				$("form").find(".spinner").removeClass("spinner");
			} , {
				enableHighAccuracy: true
			});
		}
		return false;
	}

	/* Call getLocation() each time createPost is loaded */
	$("#updateLocation").on("click", function(e){
		$("form h4").addClass("spinner");
		$(".post-submit").prop("disabled", true).removeClass("btn-primary").addClass("btn-grey");
		getLocation();
	});
	if( $("body").find("#createPost").length ) {
		getLocation();
	}

	/* BEGIN Google Maps */
	if ( $("body").find("#map").length ) {
		var mapCenter = new google.maps.LatLng($(".lat").html(), $(".lng").html());
		var map = new google.maps.Map(document.getElementById("map"), {
			center: mapCenter,
			zoom: 15,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
			scrollwheel: false
		});
		var marker = new google.maps.Marker({
			position: mapCenter,
			map: map
		});
		var infowindow = new google.maps.InfoWindow({
			content: $(".address").html()
		});
		// infowindow.open(map,marker);
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map, marker);
		});
	}
	/* END Google Maps */

	/* Copypaste helper parsing function */
	function getURLParameter(url, name) {
	    return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
	}

	/* Delete post routine */
	$(".delete").on("click", function(event){
		event.preventDefault();
		if ( confirm("Do you really want to delete this post?") ) {
			var url = $(this).attr("href");
			var id = getURLParameter(url, 'id');

			/*$(this).parent().parent().slideUp(400, function(){
				$.ajax({
					type: "POST",
					url: "/delete/",
					dataType: "text",
					data: {'id' : id},
					success: function(){
						
					}
				});
			});*/

			$.ajax({
				type: "POST",
				url: "/delete/",
				dataType: "text",
				data: {'id' : id},
				success: function(){
					window.location.href = "/";
				}
			});
		}
	});

	/* Vouch/unvouch buttons behaviour */
	$(".vouch").on("click", function(event){
		event.preventDefault();
		var href = this.href;

		$(this).toggleClass("unvouch");
		$(this).hasClass("unvouch") ? $(this).text("Unvouch") : $(this).text("Vouch");

		location.href = href;
	});

	$(".task").hover(function(){
		$(this).addClass("hover");
	}, function(){
		$(this).removeClass("hover");
	});
	$(".task .panel-body").on("click", function() {
		window.location = "/post/"+$(this).parent().attr("data");
	});

	$(".dropdown-filter li a").on("click", function(event){
		event.preventDefault();
		filter($(this).attr("data"));
		$(this).parents().find(".dropdown-filter button").html($(this).text()+' <span class="caret"></span>');
	});

	// $.ajax({
	// 	type: "POST",
	// 	url: "/post-new",
	// 	dataType: "JSON",
	// 	data: {
	// 		title: "shit",
	// 		msg: "shit",
	// 		price: "1.5",
	// 		latitude: "50.464208899999996",
	// 		longtitude: "30.466489",
	// 		address: "shit"
	// 	}
	// });

	/* Create post routine */
	// $("#createPost").submit(function(event){
	// 	event.preventDefault();
	// 	var title, message, price, lat, lng, address;

	// 	title = $(this).find("input[name=title]").val();
	// 	message = $(this).find("textarea[name=text]").val();
	// 	price = $(this).find("input[name=price]").val();
	// 	lat = $(this).find("input[name=latitude]").val();
	// 	lng =  $(this).find("input[name=longitude]").val();
	// 	address = $(this).find("input[name=address]").val();

	// 	console.log(title + "\n" + message + "\n" + price + "\n" + lat + "\n" + lng + "\n" + address);
		
	// 	var url = window.location.search.substring(1);
	// 	var id = url.split("=");

	// 	$.ajax({
	// 		type: "POST",
	// 		url: "/post-new/",
	// 		dataType: "text",
	// 		data: {
	// 			title: title,
	// 			msg: message,
	// 			price: price,
	// 			latitude: lat,
	// 			longitude: lng,
	// 			location: address
	// 		},
	// 		success: function(){
	// 			window.location.href = "/post/all"
	// 		}
	// 	});
	// });
});