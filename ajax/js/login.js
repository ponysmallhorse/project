$(function() {
    $("#loginForm").submit(function(event){
        event.preventDefault();
        var email = $(event.target).find("input[name='email']").val();
        var password = $(event.target).find("input[name='password']").val();
        var data = "email=" + email + "\u0026password=" + password;
        //alert(data);
        
        
        $.support.cors = true;
        $.ajax({
          type: "POST",
          crossDomain: true,
          async: true,
          url: "https://project-30-04.appspot.com/login",
          xhrFields: {
            withCredentials: true
            },
          data: data,
          success: function(data, textStatus, request){
            console.log(data);
            console.log(request.getAllResponseHeaders());
            console.log(request.getResponseHeader("custom"));
            $.cookie("user_id", request.getResponseHeader("custom"));
          }
        });


    });
});