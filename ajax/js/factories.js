angular
.module('fetch')
.factory('Posts', function(){
    return [{
        id: 0,
        author: "Olga Bavenko",
        title: 'Доставить браслет на м. Льва Толстого',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque efficitur ac diam in ultrices.',
        price: 4,
        address: 'Ентузіастів 9/1',
        vouchers: [],
        status: 'ready'
    },{
        id: 1,
        author: "Alexander Zinchenko",
        title: 'Хочу кофе. Большой латте',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque efficitur ac diam in ultrices.',
        price: 7,
        address: 'Русановский бульвар 10',
        vouchers: [1],
        status: 'own'
    },{
        id: 2,
        author: "Marina Kraytor",
        title: 'Сходите за продуктами в Сильпо или АТБ',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque efficitur ac diam in ultrices.',
        price: 13,
        address: 'Алексея Давыдова 4',
        vouchers: [1, 2, 3],
        status: 'activity'
    },{
        id: 3,
        author: "Evgeniy Zinchenko",
        title: 'Срочно! Подвезите в центр, пожалуйста',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque efficitur ac diam in ultrices.',
        price: 6,
        address: 'Серафимовича 11',
        vouchers: [],
        status: 'activity'
    },{
        id: 4,
        author: "John Connor",
        title: 'Биг Мак и большую колу',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque efficitur ac diam in ultrices.',
        price: 9,
        address: 'Флоренции 5',
        vouchers: [],
        status: 'ready'
    },{
        id: 5,
        author: "Arnold Schwarzenegger",
        title: 'Нужно 2 мешка цемента с доставкой',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque efficitur ac diam in ultrices.',
        price: 20,
        address: 'Русановская Набережная 12',
        vouchers: [1, 2, 3, 4, 5, 6, 7, 8, 9],
        status: 'activity'
    }]
})
