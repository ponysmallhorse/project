
angular
.module('fetch', ['ui.router', 'angularRangeSlider'/*, 'hj.uiSrefFastclick'*/])
.config(function($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise('/');

    $stateProvider
    .state('login', {
        url: '/login',
        templateUrl: 'login.html'
    })
    .state('home', {
        abstract: true,
        url: '/',
        templateUrl: 'home.html',
        controller: 'FetchCtrl'
    })
    .state('home.activity', {
        url: '',
        views: {
            'header@home': { templateUrl: 'templates/partials/header.html' },
            'subheader@home': { templateUrl: 'templates/partials/activity/subheader.html' },
            'content@home': {
                templateUrl: 'templates/activity.html',
                controller: ActivityCtrl
            }
        }
    })
        .state('home.activity.post', {
            url: 'activity/:id',
            views: {
                'header@home': { templateUrl: 'templates/partials/header-deep.html' },
                'content@home': {
                    templateUrl: 'templates/post.html',
                    controller: PostCtrl
                }
            }
        })
    .state('home.ready', {
        url: 'ready',
        views: {
            'header@home': { templateUrl: 'templates/partials/header.html' },
            'content@home': {
                templateUrl: 'templates/activity.html',
                controller: ActivityCtrl
            }
        }
    })
    .state('home.own', {
        url: 'own',
        views: {
            'header@home': { templateUrl: 'templates/partials/header.html' },
            'subheader@home': { templateUrl: 'templates/partials/own/subheader.html' },
            'content@home': {
                templateUrl: 'templates/activity.html',
                controller: ActivityCtrl
            }
        }
    })
    .state('home.profile', {
        url: 'profile',
        views: {
            'header@home': { templateUrl: 'templates/partials/header.html' },
            'content@home': { templateUrl: 'templates/profile.html' }
        }
    })
    .state('posts', {
        abstract: true,
        url: '/',
        templateUrl: 'templates/posts.html'
    })
    .state('posts.create', {
        url: 'create',
        views: {
            'header@posts': { templateUrl: 'templates/partials/create/header.html' },
            'content@posts': {
                templateUrl: 'templates/create.html',
                controller: function($scope) {
                    $scope.price = 15;
                }
            }
        }
    });
})
.directive('goBack', function($window){
  return function($scope, $element){
    alert("Back!");
    $element.on('click', function(){
      $window.history.back();
    })
  }
})
.controller('FetchCtrl', function($rootScope, $state, $scope) {
    $scope.postsFilter = $state.current.name;
    $scope.postsFilter = $scope.postsFilter.replace("home.", "");
    //alert($scope.postsFilter);

    $rootScope.$state = $state;
    /* Show/hide subheader if present */
    $rootScope.subheader = $state.current.views.hasOwnProperty('subheader@home');

    $rootScope.$on('$stateChangeSuccess', 
    function(event, toState, toParams, fromState, fromParams){
        $rootScope.subheader = $state.current.views.hasOwnProperty('subheader@home');
    });
});