import random
import hashlib
from string import letters
from Post import Post

from google.appengine.ext import db

def make_salt(length = 5):
    return ''.join(random.choice(letters) for x in xrange(length))

def make_pw_hash(pw, salt = None):
    if not salt:
        salt = make_salt()
    h = hashlib.sha256(pw + salt).hexdigest()
    return '%s,%s' % (salt, h)

def valid_pw(password, h):
    salt = h.split(',')[0]
    return h == make_pw_hash(password, salt)

def users_key(group = 'default'):
    return db.Key.from_path('users', group)

class User(db.Model):
    name = db.StringProperty(required = True)
    pw_hash = db.StringProperty(required = True)
    email = db.StringProperty(required = True)
    posts_owned = db.StringListProperty()
    ongoing = db.StringProperty()
    vouched = db.StringListProperty()

    @classmethod
    def by_id(cls, uid):
        return User.get_by_id(uid, parent = users_key())

    @classmethod
    def by_name(cls, name):
        u = User.all().filter('name =', name).get()
        return u

    @classmethod
    def by_mail(cls, email):
        u = User.all().filter('email =', email).get()
        return u

    @classmethod
    def register(cls, name, pw, email = None):
        pw_hash = make_pw_hash(pw)
        return User(parent = users_key(),
                    name = name,
                    pw_hash = pw_hash,
                    email = email,
                    vouched = [],
                    posts_owned = [],
                    ongoing = '')

    @classmethod
    def login(cls, email, pw):
        u = cls.by_mail(email)
        if u and valid_pw(pw, u.pw_hash):
            return u

    @property
    def id(self):
    	return self.key().id()

    def owned_posts(self):
        owned = []
        for x in self.posts_owned:
            p = Post.get_by_id(int(x.split(', ')[0]))
            if p != None:
                owned.append(p)
        return owned

    def inf(self):
        return "%s, %s" % (str(self.id), self.name)

    def vouched_titles(self):
        return [d.split(", ")[1] for d in self.vouched]

    def vouched_ids(self):
        return [int(d.split(", ")[0]) for d in self.vouched]

    def as_dict(self):
        dict_user = {"name": self.name,
                    "email": self.email,
                    "ongoing": self.ongoing,
                    "vouched": self.vouched,
                    "posts_owned": self.posts_owned}
        return dic_user