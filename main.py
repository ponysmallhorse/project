import os
import re
import hmac
import webapp2
import jinja2
import json

from User import User
from User import db
from Post import Post

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
                               autoescape = True)

secret = 'gav_gav'

USER_RE = re.compile(ur'^[\w]{3,20} ?[\w]{0,20}', re.UNICODE)
def valid_username(username):
    return username and USER_RE.match(username)

PASS_RE = re.compile(r"^.{3,20}$")
def valid_password(password):
    return password and PASS_RE.match(password)

EMAIL_RE  = re.compile(r'^[\S]+@[\S]+\.[\S]+$')
def valid_email(email):
    return email and EMAIL_RE.match(email)
    # 
def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)

def make_secure_val(val):
    return '%s|%s' % (val, hmac.new(secret, val).hexdigest())

def check_secure_val(secure_val):
    val = secure_val.split('|')[0]
    if secure_val == make_secure_val(val):
        return val

class MainHandler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        params['user'] = self.user
        return render_str(template, **params)

    def render_json(self, d):
        json_txt = json.dumps(d)
        self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
        self.write(json_txt)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

    def set_secure_cookie(self, name, val):
        cookie_val = make_secure_val(val)
        self.response.headers['custom'] = cookie_val
        self.response.headers.add_header(
            'Set-Cookie',
            '%s=%s; Path=/' % (name, cookie_val))

    def read_secure_cookie(self, name):
        cookie_val = self.request.cookies.get(name)
        return cookie_val and check_secure_val(cookie_val)

    def login(self, user):
        self.set_secure_cookie('user_id', str(user.key().id()))

    def logout(self):
        self.response.headers.add_header('Set-Cookie', 'user_id=; Path=/')

    def initialize(self, *a, **kw):
        webapp2.RequestHandler.initialize(self, *a, **kw)
        self.response.headers['Content-Type'] = 'text/html; charset=UTF-8'
        self.response.headers.add_header(
            'Access-Control-Allow-Methods',
            'GET, POST')
        self.response.headers.add_header(
            'Access-Control-Allow-Origin',
            'http://localhost:8000')
        self.response.headers.add_header(
            'Access-Control-Allow-Credentials',
            'true')
        self.response.headers.add_header(
            'Access-Control-Allow-Headers',
            '*')
        self.response.headers.add_header(
            'Access-Control-Expose-Headers',
            'custom, *')
        self.response.headers.add_header(
            'Access-Control-Request-Headers',
            'x-requested-with')
        uid = self.read_secure_cookie('user_id')
        self.user = uid and User.by_id(int(uid))

        if self.request.url.endswith('.json'):
            self.format = 'json'
        else:
            self.format = 'html'

class Login(MainHandler):
    def get(self):
        if self.user != u'' and self.user != None:
            self.redirect('/')
        self.render("login.html")

    def post(self):
        # self.response.headers.add_header(
        #     'Access-Control-Allow-Origin',
        #     '*')
        
        email = self.request.get('email')
        password = self.request.get('password')

        u = User.login(email, password)
        if u:
            self.login(u)
            # self.redirect('/.json')
        else:
            msg = 'Invalid login'
            self.render('login.html', error = msg)

class Logout(MainHandler):
    def get(self):
        self.logout()
        self.redirect('/login')

class Registration(MainHandler):
    def get(self):
        self.render('registration.html')

    def post(self):
        have_error = 'hidden'
        self.username = self.request.get('name')
        self.password = self.request.get('password')
        self.verify = self.request.get('verify')
        self.email = self.request.get('email')

        params = dict(name = self.username,
                      email = self.email)

        if not valid_username(self.username):
            params['error_username'] = "That's not a valid username."
            have_error = ''

        if not valid_password(self.password):
            params['error_password'] = "That wasn't a valid password."
            have_error = ''

        elif self.password != self.verify:
            params['error_verify'] = "Your passwords didn't match."
            have_error = ''

        if not valid_email(self.email):
            params['error_email'] = "That's not a valid email."
            have_error = ''

        if not have_error:
            params['have_error'] = have_error
            self.render('registration.html', **params)
        else:
            self.done()

    def done(self):
        u = User.by_mail(self.email)
        if u:
            msg = 'That user already exists.'
            self.render('registration.html', error_name = msg)
        else:
            u = User.register(self.username, self.password, self.email)
            u.put()

            self.redirect('/login')

class NavHandler(MainHandler):
    def dispatch(self):
        if self.user == u'' or self.user == None:
            self.redirect('/login')
        else:
            super(NavHandler, self).dispatch()

    def render_str(self, template, **params):
        params['user'] = self.user
        return render_str(template, **params)

class PostAll(NavHandler):
    def get(self):
        if self.format == "html":
            self.render('posts_all.html', user_id = self.user.id,\
                        posts = Post.all().order("-created"), \
                        user_vouched_list = self.user.vouched_ids())
        elif self.format == "json":
            posts = [p.as_dict() for p in Post.all().order("-created")]
            self.render_json(posts)

class PostId(NavHandler):
    def get(self, p_id):
        try:
            p_id = int(p_id)
        except:
            p_id = 1000000001
        p = Post.get_by_id(p_id)
        if p == None:
            self.abort(404)
        if self.format == 'html':
            self.render('post_id.html', user_id = self.user.id,\
                    post = p)
        elif self.format == 'json':
            self.render_json(p.as_dict())

class NewPost(NavHandler):
    def get(self):
        self.render('new_post.html')

    def post(self):
        self.title = self.request.get('title')
        self.text = self.request.get('text')
        self.lat = self.request.get('latitude')
        self.price = self.request.get('price')
        self.long = self.request.get('longitude')
        self.address = self.request.get('address')
        self.error_list = []

        try:
            self.price = float(self.price)
        except ValueError:
            self.error_list.append('invalid price')

        if self.title == u'' or self.title[0] == ' ':
            self.title.lstrip()
            self.error_list.append('no title')

        if not self.text:
            self.error_list.append('no text')

        if len(self.error_list) != 0:
            self.render('new_post.html', error = ', '.join(self.error_list),\
                                            title = self.title, text = self.text)
        else:
            p = Post.create(self.title, self.text, self.price, self.user, \
                self.address, self.lat, self.long)
            p.put()
            self.user.posts_owned.append(p.inf())
            self.user.put()
            self.redirect('/')

class EditPost(NavHandler):
    def get(self):
        p = Post.get_by_id(int(self.request.get('id')))
        if self.user.id == p.owner_id:
            self.render('edit_post.html', post = p)
        else:
            self.response.write('WTF')
    
    def post(self):
        p = Post.get_by_id(int(self.request.get('id')))
        new_title = self.request.get('title')
        new_text = self.request.get('text')
        if new_title != '' and new_text != '':
            p.title = new_title
            p.text = new_text
            p.put()
        else:
            p.delete()
        self.redirect('/')

class PostDelete(NavHandler):
    def post(self):
        p = Post.get_by_id(int(self.request.get('id')))
        if self.user.id == p.owner_id:
            for voucher in p.vouchers_ids():
                vouch_user = User.by_id(voucher)
                vouch_user.vouched.remove(p.inf()) 
                vouch_user.put()

            p.delete()
            self.redirect('/')
        else:
            self.abort(403)

class VouchPost(NavHandler):
    def get(self, p_id):
        p = Post.get_by_id(int(p_id))
        if not p or self.user.ongoing != u'':
            self.abort(403)
        if self.user.id != p.owner_id:
            post_info = "%s, %s" % (str(p.id), p.title)
            user_info = "%s, %s" % (str(self.user.id), self.user.name)

            if  post_info not in self.user.vouched:
                self.user.vouched.append(post_info)
                p.vouchers.append(user_info)
            else:
                try:
                    self.user.vouched.remove(post_info)
                    p.vouchers.remove(user_info)
                except:
                    self.abort(403)
            p.put() 
            self.user.put()
        self.redirect("/post/%s" % p.id)

class SelectHandler(NavHandler):
    def get(self, p_id, u_id):
        p = Post.get_by_id(int(p_id))
        u = User.by_id(int(u_id))

        for voucher in p.vouchers_ids():
            vouch_user = User.by_id(voucher)
            vouch_user.vouched.remove(p.inf()) 
            vouch_user.put()

        p.vouchers.clear()
        u.vouched.clear()

        p.selected = u.inf()
        u.ongoing = p.inf()

        p.status = "In progress"
        p.put()
        u.put()
        self.redirect("/post/%s" % p.id)

class CurrentHandler(NavHandler):
    def get(self):
        if self.user.ongoing != '' and self.user.ongoing != None:
            p = Post.get_by_id(int(self.user.ongoing.split(", ")[0]))
        else:
            p = None
        self.render('current.html', post = p)

class OwnedPosts(NavHandler):
    def get(self):
        if self.format == 'html':
            self.render("owned.html", posts = self.user.owned_posts())
        elif self.format == 'json':
            owned_posts = [p.as_dict() for p in self.user.owned_posts()]
            self.render_json(owned_posts)

class NotFound404(MainHandler):
    pass

app = webapp2.WSGIApplication([
    ('/(?:.json)?', PostAll),
    ('/login', Login),
    ('/logout', Logout),
    ('/signup', Registration),
    ('/post/([0-9]+)(?:.json)?', PostId),
    ('/delete/', PostDelete),
    ('/vouch', VouchPost),
    ('/select/?(?:.json)?', SelectHandler),
    ('/post-edit', EditPost),
    ('/current', CurrentHandler),
    ('/post-new', NewPost),
    ('/owned(?:.json)?', OwnedPosts),
    ("/*", NotFound404)
], debug=True)